module Core.Util
    ( ShowBS
    , OpenIn(Term, Open)
    , Items
    , spawn
    , hmenuPath
    , histFile
    , getSearchPath
    ) where

-- Map
import Data.Map.Strict (Map)

-- ByteString
import           Data.ByteString       (ByteString)
import qualified Data.ByteString.Char8 as BS

-- Other imports
import Control.Monad (void)
import Data.Functor ((<&>))
import System.Directory (XdgDirectory(XdgConfig), getXdgDirectory)
import System.FilePath ((</>))
import System.Posix.Env.ByteString (getEnvDefault)
import System.Process (spawnCommand)


-- | Type for an Map that describes all of the executables with their ratings.
type Items = Map ByteString Int

-- | Type for helping to decide how to open something.
data OpenIn
    = Term
    | Open

-- | ShowS for ByteString because it is shorter :>.
type ShowBS = ByteString -> ByteString

-- | Spawn a command and forget about it.
spawn :: ByteString -> IO ()
spawn = void . spawnCommand . BS.unpack

-- | Get all directories in @\$PATH@ as a list.
getSearchPath :: IO [ByteString]
getSearchPath = BS.split ':' <$> getEnvDefault "PATH" ""

-- | XDG_CONFIG_HOME
xdgConfig :: IO FilePath
xdgConfig = getXdgDirectory XdgConfig ""

-- | Path to the hmenu directory.
-- @XDG_CONFIG_HOME\/hmenu@, so probably @~\/.config\/hmenu@.
hmenuPath :: IO FilePath
hmenuPath = xdgConfig <&> (</> "hmenu")

-- | Path to the history file.
-- @~\/.config\/hmenu\/histFile@
histFile :: IO FilePath
histFile = hmenuPath <&> (</> "histFile")
