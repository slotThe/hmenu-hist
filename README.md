# hmenu-hist

`hmenu-hist` is a small wrapper for dmenu to make certain things easier.
Specifically, it is [hmenu](https://gitlab.com/slotThe/hmenu) with everything but the history feature removed.  This
makes it act like even more of a discount `yeganesh`.


# Features

Display commands in order of usage.

At the moment, all arguments that passed to `hmenu-hist` will be directly
forwarded to `dmenu` (this may change in the future when we get our own command
line options), so you may specify options in the following way:

    hmenu -i -f -nb '#282A36' -nf '#BBBBBB' -sb '#8BE9FD' -sf '#000000' -fn 'Inconsolata Regular-10'


# Installation

Build with `stack build`, then copy the executable to a convenient location.
