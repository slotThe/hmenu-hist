module Main
    ( main
    ) where

-- Local imports
import Core.Select
    ( getExecutables
    , makeNewEntries
    , runUpdate
    , selectWith
    , sortByValues
    , tryRead
    )
import Core.Util (histFile, hmenuPath)

-- Map
import qualified Data.Map.Strict as Map

-- Other imports
import System.Directory (createDirectoryIfMissing)
import System.Environment (getArgs)


{- | Execute dmenu and then do stuff.
   NOTE: Intersection and union for maps are both left biased and this fact is
         used the calculations below.
-}
main :: IO ()
main = do
    -- Command line arguments, these get passed straight to dmenu.
    opts <- getArgs

    -- Create the 'hmenu' directory (and all parents) if necessary.
    createDirectoryIfMissing True =<< hmenuPath

    -- See Note [Caching]
    -- Everything new as a map.
    execs <- makeNewEntries <$> getExecutables

    -- New map where everything old (i.e. not in the $PATH or the config
    -- anymore) is thrown out and anything new is added to the map.
    hist <- tryRead =<< histFile
    let inters = hist `Map.intersection` execs
        newMap = inters <> execs
        -- 'mappend' for maps is the union (as expected).

    -- Let the user select something from the list.
    selection <- selectWith opts (sortByValues newMap)

    -- Process output.
    case selection of
        Left  _ -> pure ()  -- silently fail
        Right s -> runUpdate s newMap

{- Note [Caching]
   ~~~~~~~~~~~~~~~~~~~~~~
   Doing the caching *after* the user has selected something may be better (in
   terms of perceived speed), though hmenu would "lag behind" for one execution
   when things are updated.  As of version 0.2.0, we're almost as fast as before
   being able to keep track of often used commands, so this is almost certainly
   a non-issue.
-}
